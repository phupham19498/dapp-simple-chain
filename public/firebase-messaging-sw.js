// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
  apiKey: "AIzaSyALBnhNXjhcnGKpqH4YYAS7jIB1h34B_lU",
  authDomain: "bami-b011c.firebaseapp.com",
  projectId: "bami-b011c",
  storageBucket: "bami-b011c.appspot.com",
  messagingSenderId: "458768934036",
  appId: "1:458768934036:web:fdff8ddd3bce4743708081"
};

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage(async function(payload) {
  try {
    console.log('Received background message ', payload);
    // Customize notification here
    const { data, notification } = payload
    let options = {
      icon: './logo.png'
    };
    if (notification) {
      const { body, title } = notification
      if (!body || !title) return;
      options.body = body
      return self.registration.showNotification(title, options);
    }
    if (data) {
      const { body, title } = data
      if (!body || !title) return;
      options.body = body
      return self.registration.showNotification(title, options);
    }
  } catch (e) {
    console.log(e)
  }
});
