import React from 'react'
import { ModalProvider , light , dark } from '@phamphu19498/bami-uikit'
import { HelmetProvider } from 'react-helmet-async'
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux'
import { LanguageProvider } from 'contexts/Localization'
import { Web3ReactProvider } from '@web3-react/core'
import { RefreshContextProvider } from 'contexts/RefreshContext'
import Web3 from 'web3'
import store from 'state'
import { ToastsProvider } from 'contexts/ToastsContext'
import { useThemeManager } from 'state/user/hooks'

function getLibrary(provider) {
  return new Web3(provider)
}
const ThemeProviderWrapper = (props) => {
  const [isDark] = useThemeManager()
  return <ThemeProvider theme={isDark ? dark : light} {...props} />
}
const Providers: React.FC = ({ children }) => {

  return (
    <Web3ReactProvider getLibrary={getLibrary}>
      <Provider store={store}>
        <ToastsProvider>
            <HelmetProvider>
                <ThemeProviderWrapper>
                    <LanguageProvider>
                        <RefreshContextProvider>
                          <ModalProvider>{children}</ModalProvider>
                        </RefreshContextProvider>
                    </LanguageProvider>
                </ThemeProviderWrapper>
            </HelmetProvider>
        </ToastsProvider>
      </Provider>
    </Web3ReactProvider>
    
         
  )
}

export default Providers
