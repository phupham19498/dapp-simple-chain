import React, { useEffect } from 'react'
import { useModal, Button } from '@phamphu19498/bami-uikit'
import { useTranslation } from 'contexts/Localization'
import { InjectedConnector } from '@web3-react/injected-connector'
import { useWeb3React } from "@web3-react/core";
import { setupNetwork } from 'hooks/useSwitchPolygon';
 
const ConnectWalletButton = (props) => {
  const { t } = useTranslation()
  const injected = new InjectedConnector({
    supportedChainIds: [137, 80001],
  })
  const { activate, deactivate } = useWeb3React()
  async function connect() {
    try {
      const hasSetup = await setupNetwork()
      if (hasSetup) {
          await activate(injected)
          localStorage.setItem('isWalletConnected', "true")
      }
    } catch (ex) {
      console.log(ex)
    }
  }
  useEffect(() => {
    async function connectWalletOnPageLoad(){
      if (localStorage?.getItem('isWalletConnected') === 'true') {
        try {
          await activate(injected)
          localStorage.setItem('isWalletConnected', "true")
        } catch (ex) {
          console.log(ex)
        }
      }
    }
    connectWalletOnPageLoad()
  },[])  // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <Button {...props} onClick={connect}>
      {t('Connect')}
    </Button>
  )
}

export default ConnectWalletButton
