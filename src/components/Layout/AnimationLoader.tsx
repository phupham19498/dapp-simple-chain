import React from "react";
import styled from "styled-components";
 
export const AnimationLoader = () => {
    return (
        <LoadingContainer>
            <Items/>
            <Items/>
            <Items/>
        </LoadingContainer>
    )
}
const LoadingContainer = styled.div`
    position: relative;
    width: 60px;
    height: 60px;
    margin: auto;
    > div:nth-child(1){
        width: 30px;
        height: 30px;
    }
    > div:nth-child(2){
        width: 50px;
        height: 50px;
        animation-delay: 0.1s;
    }
    > div:nth-child(3){
        width: 70px;
        height: 70px;
        animation-delay: 0.2s;
    }
    @keyframes spin {
        50% {
          transform: translate(-50%, -50%) rotate(180deg);
        }
        100% {
          transform: translate(-50%, -50%) rotate(0deg);
        }
    }
`
const Items = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border: 5px solid ${({ theme }) => theme.colors.TagX};
    border-radius: 50%;
    border-top-color: transparent;
    border-bottom-color: transparent;
    animation: spin 2s ease infinite;
`