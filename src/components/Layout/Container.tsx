import React from 'react'
import { Box } from '@phamphu19498/bami-uikit'



const Container = ({ children, ...props }) => (
  <Box px={['16px', '30px']} mx="auto"  {...props}>
    {children}
  </Box>
)

export default Container
