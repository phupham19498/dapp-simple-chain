import React from 'react'
import styled, { css } from 'styled-components'
import { useTranslation } from 'contexts/Localization'
import { Helmet } from 'react-helmet-async'
import { useLocation } from 'react-router'
import Container from './Container'



const ContainerPage = styled(Container) `
  ${props => props.isBackgroundImage && css`
      // background-image: url("/images/Swap_Background.png");
    `}
  ${props => !props.isBackgroundImage && css`
    background: ${props.background || '#F9FAFE'}  ;
    `}
  min-height: calc(100vh - 64px);
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-attachment: fixed;
  width:1320px;
  @media only screen and (max-width: 600px) {
    padding:0px;
  }
`

const StyledPage = styled(Container)`
  ${({ theme }) => theme.mediaQueries.sm} {
    padding-top: 24px;
    padding-bottom: 24px;
    padding-left: 0px;
    padding-right: 0px;
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    padding-top: 32px;
    padding-bottom: 32px;
  }
  max-width: none;
`
const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    width:100%;
    ${({ theme }) => theme.isDark ? 'background: #100E0C' : `background-image: url("/images/BackgroundBami/Background-repeat-x.jpg")`};
`

const PageMeta = () => {
  const { t } = useTranslation()
  const { pathname } = useLocation()
  return (
    <Helmet>
      <title>{t("Bami DApp - The World's Simplest NFT Pawn Shop")}</title>
      <meta property="og:title" content={t("Bami DApp - The World's Simplest NFT Pawn Shop")} />
      <meta property="og:description" content={t("Bami decentralized application enables users to earn passive income and make investment capital flexible with attractive features.")} />
      <meta property="og:image" content="https://develop.d3bpl1jd038wo5.amplifyapp.com/images/bami_preview.png" />
    </Helmet>
  )
}

const Page: React.FC<React.HTMLAttributes<HTMLDivElement>> = ({ children, ...props }) => {
  const isBackgroundImage = true
  return (
    <Wrapper>
      <PageMeta />
      <ContainerPage {...props} isBackgroundImage={isBackgroundImage}>
        <StyledPage {...props} isBackgroundImage={false} background="transparent">{children}</StyledPage>
      </ContainerPage>
    </Wrapper>
  )
}

export default Page
