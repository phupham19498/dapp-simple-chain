import React from 'react'
import styled from 'styled-components'
import { Box, Flex, Heading, Text } from '@phamphu19498/bami-uikit'
import { useTranslation } from 'contexts/Localization'

interface PageheaderProp {
  Page:string
  title:string
  subtitle:string
  bannerimg:string
  backgroundColor:string
  colorTitle?:string
  colorSubtitle?:string
  width?:string
  widthMobile?:string
}

const PageHeaderLaunch: React.FC<PageheaderProp> = ({ 
  Page, 
  title, 
  subtitle, 
  bannerimg, 
  backgroundColor,
  colorTitle,
  colorSubtitle,
  width,
  widthMobile
}) => {
  const { t } = useTranslation()
  return(
    <Container>
      <Row>
          <BackGround style={{background: `${backgroundColor}`}}/>
      </Row>
     <ContainerImg widthMobile={widthMobile} width={width} src={bannerimg} alt="logo" />
     <ContainerTitle>
       <CustomHeading   style={{color:`${colorTitle}`}}>{Page}</CustomHeading>
       <CustomTitle style={{color:`${colorSubtitle}`}}>{title}</CustomTitle>
       <CustomSubtitle bold >{subtitle}</CustomSubtitle>
     </ContainerTitle>
    </Container>
  )
}


export default PageHeaderLaunch

PageHeaderLaunch.defaultProps = {
  colorTitle:"#F9DFAF",
  colorSubtitle:"#130A00",
  width:"270px",
  widthMobile:"236px"
};

const Container = styled.div`
  width:100%;
  height:210px;
  padding-left:20px;
  padding-right:20px;
  background:#F9DFAF;
  box-shadow: -2px 2px 8px rgba(0, 0, 0, 0.25);
  border-radius: 5px;
  margin-bottom:20px;
  position:relative;
  @media only screen and (max-width: 600px) {
    border-radius: 0px;
    height: 340px;
    background-image: url("/images/LauchpadBami/backgroundMobile.png") !important;
    background-repeat: repeat-x;
    background-size:100% auto;
  }
`
const Row = styled(Flex)`
  position:relative;
  opacity: 0.75;
`
const BackGround = styled.div`
  width:80%;
  height:183px;
  position: absolute;
  right:0px;
  top:10px;
  @media only screen and (min-width: 600px) and (max-width: 1080px) {
    width:70%;
  }
  @media only screen and (max-width: 600px) {
    width:25%;
  }
`
const ContainerImg = styled.img<{ width: string, widthMobile:string }>`
  width: ${({ width }) => width};
  height: 184px;
  position: absolute;
  /* right: 68px; */
  right: 20px;
  top:10px;
  @media only screen and (min-width: 768px) and (max-width: 1080px) {
    right: 20px
  }
  @media only screen and (max-width: 600px) {
    width: ${({ widthMobile }) => widthMobile};
    height: 140px;
  }
`
const ContainerTitle = styled.div`
  width:725px;
  position: absolute;
  top:20px;
  left:60px;
  @media only screen and (min-width: 768px) and (max-width: 1080px) {
    width:400px;
  }
  @media only screen and (max-width: 600px) {
    top:150px;
    left:20px;
    width:305px;
  }
`
const CustomHeading = styled(Heading)`
  font-size:72px;
  color:#BB5432;
  text-transform: uppercase;
  font-weight:normal;
  text-shadow: 4px 4px 0px #000000;
  font-family: Lalezar;
  @media only screen and (max-width: 1080px) {
    font-size:36px;
    text-shadow: 2px 2px 0px #000000;
  }
  @media only screen and (max-width: 600px) {
    
  }
`
const CustomTitle = styled(Heading)`
  font-size: 36px;
  text-shadow: 2px 2px 0px #000000;
  line-height: 40px;
  font-family: Lalezar;
  text-transform: uppercase;
  @media only screen and (max-width: 1080px) {
    font-size:24px;
    text-shadow: 1px 1px 0px #000000;
    line-height: 30px;
  }
`
const CustomSubtitle = styled(Text)`
  font-size:18px;
  margin-top:10px;
  font-family: Lalezar;
  text-transform: uppercase;
  color:#000;
  @media only screen and (max-width: 1080px) {
    font-size:16px;
    margin-top:5px;
  }
`