import { MenuEntry, Colors } from '@phamphu19498/bami-uikit'
import { ContextApi } from 'contexts/Localization/types'


export interface LinkStatus {
  text: string;
  color: keyof Colors;
}

export const status = {
  LIVE: <LinkStatus>{
    text: "LIVE",
    color: "failure",
  },
  SOON: <LinkStatus>{
    text: "Coming",
    color: "warning",
  },
  NEW: <LinkStatus>{
    text: "NEW",
    color: "success",
  },
};

const config: (t: ContextApi['t']) => MenuEntry[] = (t) => [
  {
    label: t('DASHBOARD'),
    icon: 'HomeIcon',
    href: '/',
  }
]

export default config
