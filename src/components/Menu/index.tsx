import React from 'react'
import { light, Menu as UikitMenu, SwitchNetworkModal, useModal, dark } from '@phamphu19498/bami-uikit'
import { languageList } from 'config/localization/languages'
import { useTranslation } from 'contexts/Localization'
import useTheme from 'hooks/useTheme'
import config from './config'
import UserMenu from './UserMenu'

const Menu = (props) => {
  const { isDark, toggleTheme } = useTheme()
  const { currentLanguage, setLanguage, t } = useTranslation()
  return (
    <UikitMenu
      userMenu={<UserMenu/>}
      isDark={isDark}
      toggleTheme={toggleTheme}
      currentLang={currentLanguage.code}
      langs={languageList}
      setLang={setLanguage}
      // cakePriceUsd={cakePriceUsd}
      links={config(t)}
      {...props}
    />
  )
}

export default Menu
