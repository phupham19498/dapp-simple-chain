import React from 'react'
import { useWeb3React } from '@web3-react/core'
import {
  Flex,
  LogoutIcon,
  UserMenu as UIKitUserMenu,
  UserMenuItem
} from '@phamphu19498/bami-uikit'
import styled from 'styled-components';
import { useHistory } from 'react-router';
import ConnectWalletButton from 'components/ConnectWalletButton'
import { useTranslation } from 'contexts/Localization'


const UserMenu = () => {
  const { t } = useTranslation()
  const history = useHistory();
  const { account, deactivate } = useWeb3React()
  const avatarSrc =  undefined
  const MovetoYourNFts = () => {
    history.push(`/runtogethernft`)
  }
  if (!account) {
    return (
        <ConnectWalletButton scale="sm" />
    )
  }
  async function disconnect() {
    try {
      deactivate()
      localStorage.setItem('isWalletConnected', "false")
    } catch (ex) {
      console.log(ex)
    }
  }
  return (
    <CustomUIKitUserMenu account={account} avatarSrc={avatarSrc}>
      <UserMenuItem as="button">
        <Flex alignItems="center" justifyContent="space-between" width="100%" onClick={disconnect}>
            {t('Disconnect')}
          <LogoutIcon />
        </Flex>
      </UserMenuItem>
    </CustomUIKitUserMenu>
  )
}

export default UserMenu
const CustomUIKitUserMenu = styled(UIKitUserMenu)`
> div {
  overflow:hidden !important;
}
  
`