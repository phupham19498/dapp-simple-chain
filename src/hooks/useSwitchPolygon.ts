// Set of helper functions to facilitate wallet setup

import { BASE_URL, BASE_POLYGON_SCAN_URLS } from 'config'


/**
 * Prompt the user to add BSC as a network on Metamask, or switch to BSC if the wallet is on a different network
 * @returns {boolean} true if the setup succeeded, false otherwise
 */
export const setupNetwork = async () => {
    const nodes = [process.env.REACT_APP_NODE_1, process.env.REACT_APP_NODE_2, process.env.REACT_APP_NODE_3]
    const URL_EXPLORES = [BASE_POLYGON_SCAN_URLS[process.env.REACT_APP_CHAIN_ID]]
    const provider = window.ethereum
    const chainId = parseInt(process.env.REACT_APP_CHAIN_ID, 10)
    if (provider) {
        try {
        await provider.request({
            method: 'wallet_addEthereumChain',
            params: [
            {
                chainId: `0x${chainId.toString(16)}`, // chainID dang 0x...
                chainName: 'Mumbai',
                nativeCurrency: {
                  name: 'MATIC Token',
                  symbol: 'MATIC',
                  decimals: 18
                },
                rpcUrls: nodes,
                blockExplorerUrls: URL_EXPLORES
            },
            ],
        })
        return true
        } catch (error) {
        console.error('Failed to setup the network in Metamask:', error)
        return false
        }
    } else {
        console.error("Can't setup the Polygon network on metamask because window.ethereum is undefined")
        return false
    }
}

/**
 * Prompt the user to add a custom token to metamask
 * @param tokenAddress
 * @param tokenSymbol
 * @param tokenDecimals
 * @returns {boolean} true if the token has been added, false otherwise
 */
export const registerToken = async (tokenAddress: string, tokenSymbol: string, tokenDecimals: number) => {
  const tokenAdded = await window.ethereum.request({
    method: 'wallet_watchAsset',
    params: {
      type: 'ERC20',
      options: {
        address: tokenAddress,
        symbol: tokenSymbol,
        decimals: tokenDecimals,
        image: `${BASE_URL}/images/coins/${tokenAddress}.png`,
      },
    },
  })

  return tokenAdded
}
export const registerTokenUpgrade = async (Address: string, tokenSymbol: string, tokenDecimals: number, logo:string) => {
  const tokenAdded = await window.ethereum.request({
    method: 'wallet_watchAsset',
    params: {
      type: 'ERC20',
      options: {
        address: Address,
        symbol: tokenSymbol,
        decimals: tokenDecimals,
        image: `${BASE_URL}${logo}`,
      },
    },
  })

  return tokenAdded
}
