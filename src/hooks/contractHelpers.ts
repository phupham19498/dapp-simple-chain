import { ethers } from "ethers"
import contracts from "config/constants/contracts"
import { getAddress } from "utils/addressHelpers"
import mintNFTABI from "config/abi/mintNFT.json"
import { simpleRpcProvider } from 'utils/providers'

const getContract = (abi: any, address: string, signer?: ethers.Signer | ethers.providers.Provider) => {
    const signerOrProvider = signer ?? simpleRpcProvider
    return new ethers.Contract(address, abi, signerOrProvider)
  }
export const getMintNftContract = (signer?: ethers.Signer | ethers.providers.Provider) => {
    return getContract(mintNFTABI, getAddress(contracts.mintNft), signer)
}