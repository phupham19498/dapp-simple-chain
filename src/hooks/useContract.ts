import React, { useMemo } from "react"
import { getContract } from "utils"
import { Contract } from '@ethersproject/contracts'
import mintNftAbi from "config/abi/mintNFT.json"
import useActiveWeb3React from "./useActiveWeb3React"
import { getMintNftContract } from "./contractHelpers"

function useContract(address: string | undefined, ABI: any, withSignerIfPossible = true): Contract | null {
  const { library, account } = useActiveWeb3React()
 
  return useMemo(() => {
    if (!address || !ABI || !library) return null
    try {
      return getContract(address, ABI, library, withSignerIfPossible && account ? account : undefined)
    } catch (error) {
      console.error('Failed to get contract', error)
      return null
    }
  }, [address, ABI, library, withSignerIfPossible, account])
}
export function useMintNFTContract(mintNFT?: string, withSignerIfPossible?: boolean): Contract | null {
    return useContract(mintNFT, mintNftAbi, withSignerIfPossible)
}

export const useMintNftV2 = () => {
  const { library } = useActiveWeb3React()
  console.log("library", library)
  return useMemo(() => getMintNftContract(library.getSigner()), [library])
}