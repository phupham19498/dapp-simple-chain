const HEROS = [
    {
        "_id": "61dc67c8961ad73c340da864",
        "heroID": 1,
        "name": "Lucia",
        "title": "Black Widow",
        "quote": "Where are you~~? I need your heart for Master~~",
        "story": "Lucia, Lucian, or Lucy, nobody knows her exact name. She doesn't remember where she comes from but deep inside her, there's huge energy waiting to explode.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da865",
        "heroID": 2,
        "name": "X",
        "title": "Crimson Shadow",
        "quote": "No one can encroach on my territory",
        "story": "X doesn't have any memories. He was a failed experimental specimen with no memories. This outcast was rescued and raised as a human by the people of Bellmare fishing village. This might be a happy ending for X, but instantly, he went mad and massacred the entire village with his exploding energy. Being heartbroken, he fled to the abyss of Nadarzel to commit suicide but was saved by Kaos' arrow.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da866",
        "heroID": 3,
        "name": "Karla",
        "title": "The Grey Claw",
        "quote": "Fate forced me to become an assassin",
        "story": "Possessing the Demon's Circlet, Karla took over the body of a female ninja. This resurrection is to balance the world with the power of the Great Gray Witch.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da867",
        "heroID": 4,
        "name": "Hunken",
        "title": "Sacred Lancer",
        "quote": "When you gaze into the abyss, the abyss gazes back!.",
        "story": "A childhood friend with Lord Sieg. After the Vergas incident, Hunken was pushed to the Thalathan Oasis by a tornado.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da868",
        "heroID": 5,
        "name": "Dart",
        "title": "Iron Will",
        "quote": "Muscle of Iron, Bone of Steel..",
        "story": "The most famous swordsman in the Cloudspire Realm. Possessing the fanciful power of the Conqueror Sword, Dart can easily cut through a mountain and road with his unique technique.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da869",
        "heroID": 6,
        "name": "Balthazar",
        "title": "The Necromancer",
        "quote": "Let if be all that we see or seem..",
        "story": "Leader of the Oni Tribe, a tribe that worships Evil and Crime. Balthazar has the ability to revive and destroy the souls of enemies. He controls the fate of bystanders easily.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da86a",
        "heroID": 7,
        "name": "Wukong",
        "title": "Monkey King",
        "quote": "I hope they're stronger than me.",
        "story": "Super beast of the world. Wukong loves to use his enormous power, crushing opponents to despair. Later, with Thanatos, he founded the HollowBreaker organization with the infamous \"Three-Step One-Kill\" battle technique.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da86b",
        "heroID": 8,
        "name": "Anub",
        "title": "Wrath Sanction",
        "quote": "Failure of creation, Eden...",
        "story": "The courtier of Queen Zenith. He always comes up with initiatives to help reform the living condition here. Anub holds the Sutra of Longevity - one of the most dangerous ancient magic in the world.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da86c",
        "heroID": 9,
        "name": "Zandalor",
        "title": "Purist Bamboofat",
        "quote": "Gupa gupa. Do not worry my dear children. The Gupa Tribe will thrive. Gupa gupa.",
        "story": "Gupa tribe's leader. He is very good at using poison and elves' magic. His character is as worse as the way he attacks.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da86d",
        "heroID": 10,
        "name": "Yol",
        "title": "Fate Dissector",
        "quote": "Cultivation requires enlightenment to become the strongest.",
        "story": "A living guru evangelizing in Cloudspire. Yol is the one who predicted the collapse of Zenith Tower.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da86e",
        "heroID": 11,
        "name": "Amuka",
        "title": "Dragon's Son",
        "quote": "Resilient, strong and determined!",
        "story": "Yagami's adversary in the World Best Fighter. Even though Aeon was turned upside down due to the Vergas event, the only thing he cares about is to defeat Yagami at all cost.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da86f",
        "heroID": 12,
        "name": "Lenus",
        "title": "Blade of Fervor",
        "quote": "Many wield swords, but I am the only swordmaster.",
        "story": "One of the Five Greatest Swordman of the world. Obtaining the title of the Best Swordman, she has practiced swordmanship since her youth and defeated a lot of great swordmaster.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da870",
        "heroID": 13,
        "name": "Sena",
        "title": "Apprentice Wizard",
        "quote": "Even your own breath can deceive you sometimes.",
        "story": "A sorcerer apprentice. Lloyd's little sister. She is very good at spellcasting, though an apprentice is still an apprentice.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da871",
        "heroID": 14,
        "name": "Lonah",
        "title": "Fortune Teller",
        "quote": "Only those who doesn't think of stabbing people in the back, are the bloodiest murderers.",
        "story": "Lonah is a person of principles who is always attentive to details and keeps all her mind on her work. When Lonah was 7 years old, she had to endured the pain of losing her father and her beloved little sister. She dedicated her life to revenge on Ladja.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da872",
        "heroID": 15,
        "name": "Fangorn",
        "title": "Eden Protector",
        "quote": "Be it past or future. As long as I live, Eden will always have peace.",
        "story": "The protector of the Pine Forest. In the past, Fangorn used to be a god, however, because of his love for the Eden people, he decided to leave Cloudspire.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da873",
        "heroID": 16,
        "name": "Harley",
        "title": "Quinn",
        "quote": "Fate has no forgiveness for those who dare stand against it",
        "story": "A girl who loves to dress up and be praised for her singing voice. It's too bad that she is madly into magic which has caused her to shrink to her current mini-size.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da874",
        "heroID": 17,
        "name": "Ragnar",
        "title": "Flyleaf",
        "quote": "Feather is a love-gift. It is also a weapon of Iluvada to fight against the invaders.",
        "story": "Ragnar was rescued by Clemens and the company when he was captured by the Oni clan, in order to have him served as a sacrifice.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da875",
        "heroID": 18,
        "name": "Thanatos",
        "title": "The Deceiver",
        "quote": "Welcome to HollowBreaker. Here is where you will become the strongest champions",
        "story": "The petite leader of HollowBreaker obtains unknown power hidden behind his leather jacket. Those who knows the true face of Thanatos are all dead.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da876",
        "heroID": 19,
        "name": "Clemens",
        "title": "The Wanderer",
        "quote": "Is there anyone who knows English?",
        "story": "A guy who got thrown into Aeon world through the spooky gate created by the 3 Grandmaster. Luckily, Clemens has a very good Ancient Language understanding, thus he can cope with Aeon world quickly.",
        "__v": 0
    },
    {
        "_id": "61dc67c8961ad73c340da877",
        "heroID": 20,
        "name": "Leah",
        "title": "Woodpecker",
        "quote": "I'm not just some plucky girl you can string along",
        "story": "Ragnar's friend. The two had feelings for each other but were separated by their families. The gap among the social classes in Iluvada was too big, only leaving can give both complete happiness.",
        "__v": 0
    }
]

const ITEMS = [
    {
        "_id": "61d69f0251f3041e082114f3",
        "id": 1,
        "name": "Dirk",
        "type": "Basic",
        "stat1": "3 Attack Damage",
        "stat2": "",
        "codeStat1": "ad",
        "valueStat1": 3,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114f4",
        "id": 2,
        "name": "Wooden Bow",
        "type": "Basic",
        "stat1": "10% Attack Speed",
        "stat2": "",
        "codeStat1": "as",
        "valueStat1": 10,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114f5",
        "id": 3,
        "name": "Voodoo Wand",
        "type": "Basic",
        "stat1": "5 Ability Power",
        "stat2": "",
        "codeStat1": "ap",
        "valueStat1": 5,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114f6",
        "id": 4,
        "name": "Shiira's Teardrop",
        "type": "Basic",
        "stat1": "3 Mana",
        "stat2": "",
        "codeStat1": "mana",
        "valueStat1": 3,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114f7",
        "id": 5,
        "name": "Leather Armor",
        "type": "Basic",
        "stat1": "7 Armor",
        "stat2": "",
        "codeStat1": "armor",
        "valueStat1": 7,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114f8",
        "id": 6,
        "name": "Ancestor's Gift",
        "type": "Basic",
        "stat1": "7 Magic Resist",
        "stat2": "",
        "codeStat1": "mr",
        "valueStat1": 7,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114f9",
        "id": 7,
        "name": "Belt of Pirates",
        "type": "Basic",
        "stat1": "5 HP",
        "stat2": "",
        "codeStat1": "hp",
        "valueStat1": 5,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114fa",
        "id": 8,
        "name": "Gloves of Loyalty",
        "type": "Basic",
        "stat1": "10% Crit Damage",
        "stat2": "",
        "codeStat1": "crit",
        "valueStat1": 10,
        "codeStat2": "",
        "valueStat2": null,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114fb",
        "id": 9,
        "name": "Destiny Blade",
        "type": "Blade",
        "stat1": "12 Attack Damage",
        "stat2": "",
        "codeStat1": "ad",
        "valueStat1": 12,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 1,
        "item2": 1,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114fc",
        "id": 10,
        "name": "Bone Crusher",
        "type": "Broadsword",
        "stat1": "6 Attack Damage",
        "stat2": "15% Attack Speed",
        "codeStat1": "ad",
        "valueStat1": 6,
        "codeStat2": "as",
        "valueStat2": 15,
        "item1": 1,
        "item2": 2,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114fd",
        "id": 11,
        "name": "Jambiya",
        "type": "Dagger",
        "stat1": "6 Attack Damage",
        "stat2": "10 Ability Power",
        "codeStat1": "ad",
        "valueStat1": 6,
        "codeStat2": "ap",
        "valueStat2": 10,
        "item1": 1,
        "item2": 3,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114fe",
        "id": 12,
        "name": "Restorer",
        "type": "Dagger",
        "stat1": "6 Attack Damage",
        "stat2": "5 Mana",
        "codeStat1": "ad",
        "valueStat1": 6,
        "codeStat2": "mana",
        "valueStat2": 5,
        "item1": 1,
        "item2": 4,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e082114ff",
        "id": 13,
        "name": "Medallium",
        "type": "Accesories",
        "stat1": "6 Attack Damage",
        "stat2": "15 Armor",
        "codeStat1": "ad",
        "valueStat1": 6,
        "codeStat2": "armor",
        "valueStat2": 15,
        "item1": 1,
        "item2": 5,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211500",
        "id": 14,
        "name": "Pyke",
        "type": "Pike",
        "stat1": "6 Attack Damage",
        "stat2": "15 Magic Resist",
        "codeStat1": "ad",
        "valueStat1": 6,
        "codeStat2": "mr",
        "valueStat2": 15,
        "item1": 1,
        "item2": 6,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211501",
        "id": 15,
        "name": "Moment of Courage",
        "type": "Pike",
        "stat1": "6 Attack Damage",
        "stat2": "10 HP",
        "codeStat1": "ad",
        "valueStat1": 6,
        "codeStat2": "hp",
        "valueStat2": 10,
        "item1": 1,
        "item2": 7,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211502",
        "id": 16,
        "name": "Heartbreaker",
        "type": "Dagger",
        "stat1": "5 Attack Damage",
        "stat2": "20% Crit Damage",
        "codeStat1": "ad",
        "valueStat1": 5,
        "codeStat2": "crit",
        "valueStat2": 20,
        "item1": 1,
        "item2": 8,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211503",
        "id": 17,
        "name": "Alphatic Firepower",
        "type": "Gun",
        "stat1": "30% Attack Speed",
        "stat2": "",
        "codeStat1": "as",
        "valueStat1": 30,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 2,
        "item2": 2,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211504",
        "id": 18,
        "name": "Zeal blade",
        "type": "Blade",
        "stat1": "15% Attack Speed",
        "stat2": "10 Aiblity Power",
        "codeStat1": "as",
        "valueStat1": 15,
        "codeStat2": "ap",
        "valueStat2": 10,
        "item1": 2,
        "item2": 3,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211505",
        "id": 19,
        "name": "Discipline",
        "type": "Lethal Blade",
        "stat1": "15% Attack Speed",
        "stat2": "5 Mana",
        "codeStat1": "as",
        "valueStat1": 15,
        "codeStat2": "mana",
        "valueStat2": 5,
        "item1": 2,
        "item2": 4,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211506",
        "id": 20,
        "name": "Dragon Band",
        "type": "Heavy Armor",
        "stat1": "15% Attack Speed",
        "stat2": "15 Armor",
        "codeStat1": "as",
        "valueStat1": 15,
        "codeStat2": "armor",
        "valueStat2": 15,
        "item1": 2,
        "item2": 5,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211507",
        "id": 21,
        "name": "Balista",
        "type": "Bow",
        "stat1": "15% Attack Speed",
        "stat2": "15 Magic Resist",
        "codeStat1": "as",
        "valueStat1": 15,
        "codeStat2": "mr",
        "valueStat2": 15,
        "item1": 2,
        "item2": 6,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211508",
        "id": 22,
        "name": "Gem of Memories",
        "type": "Accesories",
        "stat1": "15% Attack Speed",
        "stat2": "10 HP",
        "codeStat1": "as",
        "valueStat1": 15,
        "codeStat2": "hp",
        "valueStat2": 10,
        "item1": 2,
        "item2": 7,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211509",
        "id": 23,
        "name": "Wingfield",
        "type": "Bow",
        "stat1": "15% Attack Speed",
        "stat2": "20% Crit Damage",
        "codeStat1": "as",
        "valueStat1": 15,
        "codeStat2": "crit",
        "valueStat2": 10,
        "item1": 2,
        "item2": 8,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821150a",
        "id": 24,
        "name": "Magician Cape",
        "type": "Hat",
        "stat1": "20 Ability Power",
        "stat2": "",
        "codeStat1": "ap",
        "valueStat1": 20,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 3,
        "item2": 3,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821150b",
        "id": 25,
        "name": "Fortune Striker",
        "type": "Wand",
        "stat1": "10 Ability Power",
        "stat2": "5 Mana",
        "codeStat1": "ap",
        "valueStat1": 10,
        "codeStat2": "mana",
        "valueStat2": 5,
        "item1": 3,
        "item2": 4,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821150c",
        "id": 26,
        "name": "Gigamesh",
        "type": "Accesories",
        "stat1": "10 Ability Power",
        "stat2": "15 Armor",
        "codeStat1": "ap",
        "valueStat1": 10,
        "codeStat2": "armor",
        "valueStat2": 15,
        "item1": 3,
        "item2": 5,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821150d",
        "id": 27,
        "name": "Merlini Scepter",
        "type": "Wand",
        "stat1": "10 Ability Power",
        "stat2": "15 Magic Resist",
        "codeStat1": "ap",
        "valueStat1": 10,
        "codeStat2": "mr",
        "valueStat2": 15,
        "item1": 3,
        "item2": 6,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821150e",
        "id": 28,
        "name": "Sacred Tome",
        "type": "Book",
        "stat1": "10 Ability Power",
        "stat2": "10 HP",
        "codeStat1": "ap",
        "valueStat1": 10,
        "codeStat2": "hp",
        "valueStat2": 10,
        "item1": 3,
        "item2": 7,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821150f",
        "id": 29,
        "name": "Vasterclaw",
        "type": "Fist",
        "stat1": "10 Ability Power",
        "stat2": "20% Crit Damage",
        "codeStat1": "ap",
        "valueStat1": 10,
        "codeStat2": "crit",
        "valueStat2": 20,
        "item1": 3,
        "item2": 8,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211510",
        "id": 30,
        "name": "Aeon Duster",
        "type": "Wand",
        "stat1": "10 Mana",
        "stat2": "",
        "codeStat1": "mana",
        "valueStat1": 10,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 4,
        "item2": 4,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211511",
        "id": 31,
        "name": "Lotus Sphere",
        "type": "Wand",
        "stat1": "5 Mana",
        "stat2": "15 Armor",
        "codeStat1": "mana",
        "valueStat1": 5,
        "codeStat2": "armor",
        "valueStat2": 15,
        "item1": 4,
        "item2": 5,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211512",
        "id": 32,
        "name": "Heavenly Grace",
        "type": "Wand",
        "stat1": "5 Mana",
        "stat2": "15 Magic Resist",
        "codeStat1": "mana",
        "valueStat1": 5,
        "codeStat2": "mr",
        "valueStat2": 15,
        "item1": 4,
        "item2": 6,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211513",
        "id": 33,
        "name": "Hero's Pendant",
        "type": "Accesories",
        "stat1": "5 Mana",
        "stat2": "10 Heal",
        "codeStat1": "mana",
        "valueStat1": 5,
        "codeStat2": "hp",
        "valueStat2": 10,
        "item1": 4,
        "item2": 7,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211514",
        "id": 34,
        "name": "Pawist",
        "type": "Fist",
        "stat1": "5 Mana",
        "stat2": "20% Crit Damage",
        "codeStat1": "mana",
        "valueStat1": 5,
        "codeStat2": "crit",
        "valueStat2": 20,
        "item1": 4,
        "item2": 8,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211515",
        "id": 35,
        "name": "Bloodborn",
        "type": "Lethal Blade",
        "stat1": "30 Armor",
        "stat2": "",
        "codeStat1": "armor",
        "valueStat1": 30,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 5,
        "item2": 5,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211516",
        "id": 36,
        "name": "Fairy Slasher",
        "type": "Blade",
        "stat1": "15 Armor",
        "stat2": "15 Magic Resist",
        "codeStat1": "armor",
        "valueStat1": 15,
        "codeStat2": "mr",
        "valueStat2": 15,
        "item1": 5,
        "item2": 6,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211517",
        "id": 37,
        "name": "Defiance",
        "type": "Accesories",
        "stat1": "10 HP",
        "stat2": "15 Armor",
        "codeStat1": "hp",
        "valueStat1": 10,
        "codeStat2": "armor",
        "valueStat2": 15,
        "item1": 5,
        "item2": 7,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211518",
        "id": 38,
        "name": "Horn Soul",
        "type": "Light Armor",
        "stat1": "20% Crit Damage",
        "stat2": "15 Armor",
        "codeStat1": "crit",
        "valueStat1": 20,
        "codeStat2": "armor",
        "valueStat2": 15,
        "item1": 5,
        "item2": 8,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e08211519",
        "id": 39,
        "name": "Ancestor's Faith",
        "type": "Light Armor",
        "stat1": "30 Magic Resist",
        "stat2": "",
        "codeStat1": "mr",
        "valueStat1": 30,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 6,
        "item2": 6,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821151a",
        "id": 40,
        "name": "Wind Dancer",
        "type": "Broadsword",
        "stat1": "10 HP",
        "stat2": "15 Magic Resist",
        "codeStat1": "hp",
        "valueStat1": 10,
        "codeStat2": "mr",
        "valueStat2": 15,
        "item1": 6,
        "item2": 7,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821151b",
        "id": 41,
        "name": "Cloak of Invincibility",
        "type": "Light Armor",
        "stat1": "20% Crit Damage",
        "stat2": "15 Magic Resist",
        "codeStat1": "crit",
        "valueStat1": 20,
        "codeStat2": "mr",
        "valueStat2": 15,
        "item1": 6,
        "item2": 8,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821151c",
        "id": 42,
        "name": "Void Cuirass",
        "type": "Heavy Armor",
        "stat1": "20 HP",
        "stat2": "",
        "codeStat1": "hp",
        "valueStat1": 20,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 7,
        "item2": 7,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821151d",
        "id": 43,
        "name": "Void Harmony",
        "type": "Heavy Armor",
        "stat1": "20% Crit Damage",
        "stat2": "10 HP",
        "codeStat1": "crit",
        "valueStat1": 20,
        "codeStat2": "hp",
        "valueStat2": 10,
        "item1": 7,
        "item2": 8,
        "__v": 0
    },
    {
        "_id": "61d69f0251f3041e0821151e",
        "id": 44,
        "name": "Void Gauntlet",
        "type": "Heavy Armor",
        "stat1": "40% Crit Damage",
        "stat2": "",
        "codeStat1": "crit",
        "valueStat1": 40,
        "codeStat2": "",
        "valueStat2": null,
        "item1": 8,
        "item2": 8,
        "__v": 0
    }
]

const RUNES = [
    {
        "_id": "61cc0aae53ade6256c253af0",
        "id": 1,
        "name": "Apprentice's Stone",
        "des": "Every time an enemy is killed, the Hero accumulates 1 point of Bloodthirsty. Up to 100 points. For each point of Bloodthirsty, Hero gains +1 Bonus Attack Damage.Carrying more Gems of the same type will increase the number of Bloodthirsty points in each hoard. The effect will be reset every Round.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af1",
        "id": 2,
        "name": "Hunter's Treasure",
        "des": "Hero's basic attacks deal 1% of the target's missing health.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af2",
        "id": 3,
        "name": "Regeneration Rune",
        "des": "When the champion possessing this gem has 20% health left, the champion will recover 20% of its maximum health over 5s.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af3",
        "id": 4,
        "name": "Astral Rune",
        "des": "Hits will have a 20% chance of marking Enchantment on the target.\nMarks broken by an ability will cause the enemy to be enchanted for 2s.\nEnchantment - Enchanted targets will reverse and lose ability to cast.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af4",
        "id": 5,
        "name": "Wicked Charm",
        "des": "Targets hit by a champion possessing this gem will burn 30% of the skill's damage for 5s.\nAnd take the Curse effect for the time being.\n\n\nCurse - Grave Wound reduces the target's healing by 40%.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af5",
        "id": 6,
        "name": "Ancestor's Relic",
        "des": "Gain 2 mana for each enemy champion on the way to death.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af6",
        "id": 7,
        "name": "Juxtapose Chipped",
        "des": "Within 10 seconds of spawning, when the health drops below 20%. Gems will activate Clone Clone\n\n\nClone - Champion splits into 2 forms and cannot be affected while cloned.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af7",
        "id": 8,
        "name": "Guardian Sign",
        "des": "When moving on the road, the owning champion will reduce the damage taken by 10%.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af8",
        "id": 9,
        "name": "Lionheart",
        "des": "Champions standing around the champion possessing this rune will all gain 12% attack speed.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253af9",
        "id": 10,
        "name": "Joomong's Eyelash",
        "des": "All basic attacks cannot be missed",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253afa",
        "id": 11,
        "name": "Chain Lightning",
        "des": "The possessor will launch 1 lightning after every 5 basic attacks, the lightning will spread to 4 enemies behind. The lightning will deal 15% AP of the owning champion.\n\n\nEach additional Gem of the same type will increase the target by 2.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253afb",
        "id": 12,
        "name": "Megamorph Sphere",
        "des": "Owned champions when attacked will store 1 extra Charge, each Charge will increase 2% armor and magic resistance.\nThe maximum number of charges is 25.\n\n\nEach additional Gem of the same type will increase the Charge when stored by 1.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253afc",
        "id": 13,
        "name": "Multishot",
        "des": "Physical attacks will fire 2 extra beams to attack nearby targets.\nExtra ray deals 20% of original damage. The secondary beam also applies on-hit effects.\n\n\nEach additional Gem of the same type will have the effect of increasing 1 ray.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253afd",
        "id": 14,
        "name": "Talisman of Demolition",
        "des": "Physical attacks ignore 30% of the target's armor.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253afe",
        "id": 15,
        "name": "Inquisitor's Command",
        "des": "The skill hits the target, will jolt lightning to 2 nearby enemies and deal damage.\nEach lightning bolt deals 25% magic damage.\n\n\nEach additional Gem of the same type will increase the target by 1.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253aff",
        "id": 16,
        "name": "Seal of Refraction",
        "des": "Possessing champions will gain 1 layer of virtual health equal to 15% of their maximum health.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253b00",
        "id": 17,
        "name": "Icewind Talon",
        "des": "Damage skills will create an area of ​​Frost effect for 3 seconds on enemies walking in it.\n\nFrost - Effect reduces movement speed by 30%.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253b01",
        "id": 18,
        "name": "Magic Critical",
        "des": "The champion possessing this gem will help the skill that can crit. Critical rate and crit damage will be calculated according to % crit and % crit damage possessed by the host champion.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253b02",
        "id": 19,
        "name": "Critical Block",
        "des": "Possessing champions have a 15% increased chance to block critical hits.",
        "des2": "",
        "__v": 0
    },
    {
        "_id": "61cc0aae53ade6256c253b03",
        "id": 20,
        "name": "Frost Armor",
        "des": "Damaged by physical attacks apply Numbness to enemies attacking you.\n\n\nNumbness - Hero of this effect has 20% reduced attack speed.",
        "des2": "",
        "__v": 0
    }
]

const CLASSES = [
    {
        "details": [
            "*1: +30% crit damage and 10% crit chance",
            "*3: +40% crit damage and 15% crit chance",
            "*5: +50% crit damage and 20% crit chance"
        ],
        "_id": "61ebe09a39fe8a4ba4a70998",
        "id": 1,
        "title": "Assasin",
        "desc": "Increases Critical Damage and Critical Rate by % for Assassin-type champions.",
        "__v": 0
    },
    {
        "details": [
            "*1: +20% AD",
            "*3: +25% AD",
            "*5: +30% AD"
        ],
        "_id": "61ebe09a39fe8a4ba4a70999",
        "id": 2,
        "title": "Slayer",
        "desc": "Increases AD (attack power) for Slayer-type champions",
        "__v": 0
    },
    {
        "details": [
            "*1: +3 AD and +10 AP",
            "*3: +6 AD and +15 AP",
            "*5: +9 AD and +20 AP"
        ],
        "_id": "61ebe09a39fe8a4ba4a7099a",
        "id": 3,
        "title": "Warlord",
        "desc": "Increases AD (attack power) and AP (magic power) of generals of the Warlord type",
        "__v": 0
    },
    {
        "details": [
            "*1: +20% AP",
            "*3: +30% AP",
            "*5: +40% AP"
        ],
        "_id": "61ebe09a39fe8a4ba4a7099b",
        "id": 4,
        "title": "Elderwood",
        "desc": "When active, each Ranger champion gains AP.",
        "__v": 0
    },
    {
        "details": [
            "*2: -30% armor",
            "*4: -50% armor"
        ],
        "_id": "61ebe09a39fe8a4ba4a7099c",
        "id": 5,
        "title": "Hunter",
        "desc": "When attacking, the Hunter's basic attacks deal % armor piercing damage.",
        "__v": 0
    },
    {
        "details": [
            "*2: -40% magic resistance",
            "*4: -60% magic resistance"
        ],
        "_id": "61ebe09a39fe8a4ba4a7099d",
        "id": 6,
        "title": "Warlock",
        "desc": "When attacking, Warlock's skill deals % magic resistance piercing damage.",
        "__v": 0
    },
    {
        "details": [
            "*1: champions gain 20% attack speed",
            "*3: champions gain 30% attack speed",
            "*5: champions gain 40% attack speed"
        ],
        "_id": "61ebe09a39fe8a4ba4a7099e",
        "id": 7,
        "title": "Duelist",
        "desc": "When activated, all Duelist-type champions gain attack speed.",
        "__v": 0
    },
    {
        "details": [
            "*3: +25% damage.",
            "*5: +40% damage."
        ],
        "_id": "61ebe09a39fe8a4ba4a7099f",
        "id": 8,
        "title": "Wizard",
        "desc": "When activated, the Wizard general has the ability to cast the skill twice, the second skill deals damage equal to % of the first time's damage.",
        "__v": 0
    },
    {
        "details": [
            "*2: +2 Mana on hit, +3 Mana on kill",
            "*4: +2 Mana on hit, +5 Mana on kill"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a0",
        "id": 9,
        "title": "Enlightened",
        "desc": "Each Enlightened champion gains 2 Mana per hand attack.\nWhen you kill a monster, you will gain + more mana.",
        "__v": 0
    },
    {
        "details": [
            "*3: +2 Gold"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a1",
        "id": 10,
        "title": "Fortune",
        "desc": "Fortune-type champions will gain more Gold each time they kill each enemy.",
        "__v": 0
    }
]

const ORIGINS = [
    {
        "details": [
            "*2: +20% max health",
            "*4: +35% max health"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a3",
        "id": 1,
        "title": "Brawler",
        "desc": "When activated, all champions moving on the field will gain maximum health.",
        "__v": 0
    },
    {
        "details": [
            "*2: +30% armor",
            "*4: +50% armor"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a4",
        "id": 2,
        "title": "Vanguard",
        "desc": "When activated, all Vanguard will gain additional armor while moving on the road.",
        "__v": 0
    },
    {
        "details": [
            "*2: +12% dodge",
            "*4: +20% dodge"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a5",
        "id": 3,
        "title": "Ninja",
        "desc": "When activated, all Ninja will have the opportunity to dodge the opponent's basic attacks",
        "__v": 0
    },
    {
        "details": [
            "*1: Has 30% critical block",
            "*3: Has 50% critical block",
            "*5: Has 60% critical block"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a6",
        "id": 4,
        "title": "DragonSoul",
        "desc": "When attacked, the champion will have a chance to block critical hits and reduce critical damage by 40%.",
        "__v": 0
    },
    {
        "details": [
            "*1: Take 20% less damage from all sources for 3s",
            "*3: Take 30% less damage from all sources for 3s",
            "*5: Take 35% less damage from all sources for 3s"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a7",
        "id": 5,
        "title": "Divine",
        "desc": "When health is below 20% health, take damage reduction from all sources.",
        "__v": 0
    },
    {
        "details": [
            "*3: Revive with 30% of max health"
        ],
        "_id": "61ebe09a39fe8a4ba4a709a8",
        "id": 6,
        "title": "Reviver",
        "desc": "One second after being killed, the champion respawns midway for a health equal to 40% of his maximum health proportional to the distance traveled.\nThe hero that has already resurrected cannot be resurrected again.\n\n\nNote: If the champion dies while suffering from Grievous Wounds. The amount of health after respawning will be affected by this effect.",
        "__v": 0
    },
    {
        "details": [
            "*2: 30% chance to stun for 1.5s.",
            "*4: 40% chance to stun for 1.5s."
        ],
        "_id": "61ebe09a39fe8a4ba4a709a9",
        "id": 7,
        "title": "Dazzler",
        "desc": "When damaged by an ability, the champion has a chance to stun the enemy for a period of time",
        "__v": 0
    },
    {
        "details": [
            "*1: +50% magic resistance",
            "*3: +70% magic resistance",
            "*5: +80% magic resistance"
        ],
        "_id": "61ebe09a39fe8a4ba4a709aa",
        "id": 8,
        "title": "Mystic",
        "desc": "When activated, Mystic champions moving on the road will gain magic resistance.",
        "__v": 0
    },
    {
        "details": [
            "*1: Magic shield equal to 10% of max health",
            "*3: Magic shield equal to 20% of max health",
            "*5: Magic shield equal to 25% of max health "
        ],
        "_id": "61ebe09a39fe8a4ba4a709ab",
        "id": 9,
        "title": "Guardian",
        "desc": "When spawned, the guardian champion grants a magic shield that blocks magic damage.\nWhile the shield is still on, the guardian champion is immune to all kinds of adverse effects.",
        "__v": 0
    },
    {
        "details": [
            "*2: 20% chance to burn 10 mana",
            "*4: 30% chance to burn 10 mana"
        ],
        "_id": "61ebe09a39fe8a4ba4a709ac",
        "id": 10,
        "title": "Fabled",
        "desc": "When hit, the mob has a chance to burn the hero's mana that just dealt damage to them",
        "__v": 0
    }
]

const HERO_STATS = [
    {
        "recommendItems": [
            26,
            27,
            15,
            36,
            25
        ],
        "recommendRunes": [
            6,
            3,
            16,
            15,
            8
        ],
        "recommendAlly": [
            8,
            14,
            7,
            3,
            15,
            20,
            13
        ],
        "recommendClass": [
            8,
            4
        ],
        "recommendOrigin": [
            1,
            9
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65e3",
        "heroID": 1,
        "attackDamage": 12,
        "attackSpeed": 0.7,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 50,
        "manaGain": 10,
        "heath": 100,
        "armor": 5,
        "magicResist": 20,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Fires spider web that deals 80 magic damage and immobilizes 1 the target for 3 seconds, and burns 40% current mana.\n\n* Level 3 | 5: Target count increased by 1",
        "skillRoad": "Calling out 1 baby spiders in turn right where Lucia stood.\n\n* Level 3 | 5: Target count increased by 1",
        "__v": 0
    },
    {
        "recommendItems": [
            36,
            20,
            30,
            31,
            40
        ],
        "recommendRunes": [
            20,
            8,
            16,
            7,
            17
        ],
        "recommendAlly": [
            15,
            7,
            3,
            4,
            10,
            13
        ],
        "recommendClass": [
            5,
            10
        ],
        "recommendOrigin": [
            10,
            6
        ],
        "recommendTargetType": [
            2
        ],
        "_id": "61dcf70b73e1a927480c65e4",
        "heroID": 2,
        "attackDamage": 100,
        "attackSpeed": 0.25,
        "abilityPower": 0,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 50,
        "manaGain": 10,
        "heath": 115,
        "armor": 20,
        "magicResist": 10,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Jumping up and down deals double magic damage to the target based on Attack Damage available\n- If target is Hero, kill instantly if target is below 20% max health.\n- If the target is a Boss, deals triple magic damage to the target based on Attack Damage.\n\nX instantly restores the ability if it kills the target, the damage will be reduced by 50% compared to the previous time.",
        "skillRoad": "Dives in the fog, making the enemy invisible for 3s.\nMovement speed is reduced by 20% during dive.",
        "__v": 0
    },
    {
        "recommendItems": [
            10,
            9,
            17,
            16,
            23
        ],
        "recommendRunes": [
            1,
            13,
            2,
            6,
            14
        ],
        "recommendAlly": [
            7,
            13,
            14,
            4,
            8,
            17,
            12
        ],
        "recommendClass": [
            2,
            10
        ],
        "recommendOrigin": [
            6,
            3
        ],
        "recommendTargetType": [
            2
        ],
        "_id": "61dcf70b73e1a927480c65e5",
        "heroID": 3,
        "attackDamage": 15,
        "attackSpeed": 1,
        "abilityPower": 0,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 80,
        "manaGain": 10,
        "heath": 100,
        "armor": 5,
        "magicResist": 5,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Karla gains x2 attack speed for 4s.\nDuring attack speed, Karla's basic attacks have the ability to bounce to the next nearby target up to 3 times.\nDamage after each bounce is reduced by 30% compared to before.",
        "skillRoad": "Creates a layer of armor capable of blocking 1 skill.",
        "__v": 0
    },
    {
        "recommendItems": [
            10,
            9,
            17,
            16,
            23
        ],
        "recommendRunes": [
            1,
            13,
            19,
            14,
            10
        ],
        "recommendAlly": [
            7,
            13,
            14,
            8,
            5,
            17,
            1
        ],
        "recommendClass": [
            2,
            7
        ],
        "recommendOrigin": [
            6,
            6
        ],
        "recommendTargetType": [
            1
        ],
        "_id": "61dcf70b73e1a927480c65e6",
        "heroID": 4,
        "attackDamage": 10,
        "attackSpeed": 1,
        "abilityPower": 100,
        "critChance": 0.15,
        "critDamage": 1,
        "mana": 100,
        "manaGain": 10,
        "heath": 105,
        "armor": 5,
        "magicResist": 5,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Each hit will impale the spear on the target. Each spear has the ability to deal magic damage equal to 20% of Hunken's AD.\nIf the total damage on the spears is enough to finish off the target, then Hunken will draw the spear and deal magic damage.",
        "skillRoad": "Hunken swaps positions for allies in the lead, or behind if Hunken is in the lead.",
        "__v": 0
    },
    {
        "recommendItems": [
            36,
            35,
            39,
            40,
            37
        ],
        "recommendRunes": [
            20,
            7,
            16,
            19,
            3
        ],
        "recommendAlly": [
            8,
            9,
            20,
            13,
            4,
            10,
            7
        ],
        "recommendClass": [
            2,
            3
        ],
        "recommendOrigin": [
            6,
            1
        ],
        "recommendTargetType": [
            3
        ],
        "_id": "61dcf70b73e1a927480c65e7",
        "heroID": 5,
        "attackDamage": 30,
        "attackSpeed": 0.5,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 50,
        "manaGain": 10,
        "heath": 120,
        "armor": 30,
        "magicResist": 35,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Fires a beam of magic that runs across the ground towards the target, dealing 100 magic damage, and creates a chain at that location, making it impossible for the target to go out of range.\nIf it goes out of the restricted area or 1.5s expires, the target will be dragged back to where it was.\n\n* Level 3 | 5: Target count increased by 1",
        "skillRoad": "When killed within 12(14/16/18/20) seconds since appearing, Dart will go backwards for 2s, then respawn with 10(+0.5%AP)% of his maximum health.",
        "__v": 0
    },
    {
        "recommendItems": [
            42,
            36,
            26,
            24,
            29
        ],
        "recommendRunes": [
            18,
            17,
            20,
            19,
            6
        ],
        "recommendAlly": [
            3,
            9,
            5,
            7,
            19,
            13,
            14
        ],
        "recommendClass": [
            8,
            9
        ],
        "recommendOrigin": [
            6,
            9
        ],
        "recommendTargetType": [
            2
        ],
        "_id": "61dcf70b73e1a927480c65e8",
        "heroID": 6,
        "attackDamage": 10,
        "attackSpeed": 0.7,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 60,
        "manaGain": 10,
        "heath": 100,
        "armor": 5,
        "magicResist": 10,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.7,
        "skillPlatform": "Balthazar cast a magic ball to the target's location of the currently hitting target, which explodes for 1.5s after. The explosion deals 80 magic damage to enemies within range.\nIf the location is within the influence of another magic orb, that orb will be detonated earlier and have 1s stun.\nIf the orb is donated soon, it will cause 50% additional damage (Additional damage is reduced over time).\nTarget is far from center of explostion will take less damage.",
        "skillRoad": "Newly spawned Balthazar gains 40% Movement Speed ​​for 2s.",
        "__v": 0
    },
    {
        "recommendItems": [
            31,
            30,
            32,
            9,
            10
        ],
        "recommendRunes": [
            6,
            13,
            9,
            4,
            12
        ],
        "recommendAlly": [
            10,
            3,
            19,
            9,
            20,
            6,
            13
        ],
        "recommendClass": [
            9,
            2
        ],
        "recommendOrigin": [
            6,
            2
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65e9",
        "heroID": 7,
        "attackDamage": 10,
        "attackSpeed": 0.8,
        "abilityPower": 0,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 100,
        "manaGain": 10,
        "heath": 120,
        "armor": 20,
        "magicResist": 30,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.7,
        "skillPlatform": "Wukong throws a powerful staff to block the road for a while, the opponent can't go through.\nThe ruler will disappear after 16(17/18/19/20)s or 4s after being touched by the enemy.",
        "skillRoad": "Wukong creates a clone at the current location, then becomes invisible and untargetable for 1s.\nThe hit clone will disappear.\n\n* Level 3 | 5: Hits increased by 2 | 3",
        "__v": 0
    },
    {
        "recommendItems": [
            15,
            42,
            37,
            36,
            35
        ],
        "recommendRunes": [
            8,
            16,
            3,
            12,
            13
        ],
        "recommendAlly": [
            7,
            13,
            20,
            14,
            15,
            2,
            10
        ],
        "recommendClass": [
            2,
            5
        ],
        "recommendOrigin": [
            6,
            10
        ],
        "recommendTargetType": [
            1
        ],
        "_id": "61dcf70b73e1a927480c65ea",
        "heroID": 8,
        "attackDamage": 40,
        "attackSpeed": 0.44,
        "abilityPower": 0,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 90,
        "manaGain": 10,
        "heath": 125,
        "armor": 50,
        "magicResist": 20,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.5,
        "skillPlatform": "Anub summons a sandstorm that obstructs vision of 2 targets, reducing movement speed by 80% for 4s.\n\n* Level 3 | 5: Hits increased by 3 | 4",
        "skillRoad": "Enlarges Anub, increasing max health by 15(20/25/30/35)% for 3s.",
        "__v": 0
    },
    {
        "recommendItems": [
            20,
            13,
            20,
            35,
            39
        ],
        "recommendRunes": [
            20,
            16,
            12,
            19,
            13
        ],
        "recommendAlly": [
            20,
            4,
            8,
            13,
            7,
            11,
            4
        ],
        "recommendClass": [
            7,
            1
        ],
        "recommendOrigin": [
            9,
            3
        ],
        "recommendTargetType": [
            3
        ],
        "_id": "61dcf70b73e1a927480c65eb",
        "heroID": 9,
        "attackDamage": 22,
        "attackSpeed": 1,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 100,
        "manaGain": 0,
        "heath": 100,
        "armor": 0,
        "magicResist": 0,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Zandalor's basic attack has a 20% chance to stun the target for 0.5s.\n Each time an attack has the ability to stun, Zandalor gains and stores a point of Inner Fire.\n Each additional point of Inner Fire grants Zandalor (2/4/6/8/10) AD.\n When he reaches 5 points of Inner Fire, Zandalor releases all of his stacks, causing his next attack to deal 5 times more physical damage than normal.\n \n - Zandalor will lose all of his Inner Fire if he can't store more for 2.5s.",
        "skillRoad": "Zandalor spins, gaining 40% chance to dodge basic attacks and take 20% less magic damage for 2(2.5/3/3.5/4)s.",
        "__v": 0
    },
    {
        "recommendItems": [
            17,
            10,
            23,
            19,
            18
        ],
        "recommendRunes": [
            18,
            1,
            6,
            4,
            2
        ],
        "recommendAlly": [
            19,
            7,
            14,
            13,
            3,
            4,
            8
        ],
        "recommendClass": [
            7,
            1
        ],
        "recommendOrigin": [
            10,
            4
        ],
        "recommendTargetType": [
            1
        ],
        "_id": "61dcf70b73e1a927480c65ec",
        "heroID": 10,
        "attackDamage": 10,
        "attackSpeed": 0.5,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 400,
        "manaGain": 10,
        "heath": 100,
        "armor": 5,
        "magicResist": 5,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Once full of Mana, Yol evolves to a higher level.\n* Tier 1: Yol's attacks are fan-like, capable of damaging multiple targets.\n* Tier 2: Yol's basic attacks are fan-like and deal 20% bonus magic damage on basic attacks.\nAfter each Round, Yol returns to his original form.",
        "skillRoad": "Yol becomes invulnerable, immune to all damage for 1s.",
        "__v": 0
    },
    {
        "recommendItems": [
            34,
            30,
            25,
            9,
            10
        ],
        "recommendRunes": [
            6,
            15,
            2,
            9,
            12
        ],
        "recommendAlly": [
            14,
            3,
            13,
            7,
            14,
            5,
            8
        ],
        "recommendClass": [
            2.7
        ],
        "recommendOrigin": [
            9,
            5
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65ed",
        "heroID": 11,
        "attackDamage": 23,
        "attackSpeed": 0.8,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 60,
        "manaGain": 10,
        "heath": 110,
        "armor": 20,
        "magicResist": 20,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Amuka hits the target, dealing 60 magic damage to the target and 30% magic damage to nearby enemies.\n Damaged targets will be revealed for 7s.",
        "skillRoad": "Amuka casts a spell on a shield that blocks 50 magic damage for 5s.",
        "__v": 0
    },
    {
        "recommendItems": [
            42,
            36,
            30,
            33,
            37
        ],
        "recommendRunes": [
            12,
            3,
            7,
            9,
            8
        ],
        "recommendAlly": [
            3,
            7,
            10,
            14,
            15,
            16,
            18
        ],
        "recommendClass": [
            6,
            2
        ],
        "recommendOrigin": [
            2,
            4
        ],
        "recommendTargetType": [
            3
        ],
        "_id": "61dcf70b73e1a927480c65ee",
        "heroID": 12,
        "attackDamage": 20,
        "attackSpeed": 1,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 80,
        "manaGain": 10,
        "heath": 115,
        "armor": 20,
        "magicResist": 30,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Lenus swings his sword forward, dealing 40 physical damage and stunning enemies for 2s.  * Level 3.5: Target count increased by 1",
        "skillRoad": "Lenus removes all debuffs on himself and heals for 20(22/24/26/28)% of health over 10s.",
        "__v": 0
    },
    {
        "recommendItems": [
            33,
            30,
            19,
            37,
            42
        ],
        "recommendRunes": [
            6,
            14,
            20,
            13,
            17
        ],
        "recommendAlly": [
            8,
            5,
            11,
            3,
            7,
            17,
            20
        ],
        "recommendClass": [
            9,
            8
        ],
        "recommendOrigin": [
            9,
            7
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65ef",
        "heroID": 13,
        "attackDamage": 18,
        "attackSpeed": 0.7,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 60,
        "manaGain": 10,
        "heath": 100,
        "armor": 10,
        "magicResist": 10,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Sena creates a tornado on lane 4(4.5/5/5.5/6)s.\nEnemies entering the area are slowed by 40(50/60/70/80)% Movement Speed. The slow effect cannot be dispelled.",
        "skillRoad": "Sena buffs shields to 2 nearby allies. Shield blocks 50 damage for 5s.",
        "__v": 0
    },
    {
        "recommendItems": [
            25,
            29,
            24,
            32,
            18
        ],
        "recommendRunes": [
            5,
            18,
            11,
            20,
            9
        ],
        "recommendAlly": [
            4,
            1,
            7,
            20,
            9,
            15,
            18
        ],
        "recommendClass": [
            4,
            6
        ],
        "recommendOrigin": [
            6,
            5
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65f0",
        "heroID": 14,
        "attackDamage": 10,
        "attackSpeed": 0.8,
        "abilityPower": 100,
        "critChance": 0,
        "critDamage": 0,
        "mana": 60,
        "manaGain": 10,
        "heath": 115,
        "armor": 10,
        "magicResist": 40,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Lonah creates dead ground on the way, lasting for 5s.\nDeals 50 magic damage per second and reduces magic resistance by 50% to units in it.",
        "skillRoad": "Lonah buffs a black shield on nearby allies or on herself if there are no allies nearby.\nIncreases 30(45/50/60/70)% magic resistance and control immunity for 5s.",
        "__v": 0
    },
    {
        "recommendItems": [
            40,
            37,
            36,
            42,
            15
        ],
        "recommendRunes": [
            8,
            16,
            3,
            20,
            17
        ],
        "recommendAlly": [
            7,
            13,
            8,
            14,
            20,
            4,
            5
        ],
        "recommendClass": [
            9,
            6
        ],
        "recommendOrigin": [
            1,
            9
        ],
        "recommendTargetType": [
            2
        ],
        "_id": "61dcf70b73e1a927480c65f1",
        "heroID": 15,
        "attackDamage": 10,
        "attackSpeed": 0.75,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 50,
        "manaGain": 10,
        "heath": 110,
        "armor": 20,
        "magicResist": 40,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.5,
        "skillPlatform": "Fangorn throws a Seedling on its path that lasts for 90 seconds. For every second the tree survives, it gains 1% damage.\n\nExploding seedlings deal 60 damage up to 5 nearby units and deal 60 poison damage & grievous wound effect for 5 seconds.",
        "skillRoad": "Fangorn summons a storm of leaves around himself, increasing damage reduction by 30(35/40/45/50)% for 5s and restoring 20% ​​of his missing health for 5s.",
        "__v": 0
    },
    {
        "recommendItems": [
            33,
            19,
            31,
            33,
            30
        ],
        "recommendRunes": [
            6,
            9,
            14,
            12,
            17
        ],
        "recommendAlly": [
            15,
            13,
            7,
            3,
            11,
            10,
            17
        ],
        "recommendClass": [
            9,
            5
        ],
        "recommendOrigin": [
            10,
            7
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65f2",
        "heroID": 16,
        "attackDamage": 15,
        "attackSpeed": 0.8,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 60,
        "manaGain": 10,
        "heath": 100,
        "armor": 10,
        "magicResist": 10,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.45,
        "skillPlatform": "Harley turns a target into a Sheep for 3s (Only silence the boss)\nSheep has 80% armor and magic resistance reduced, all buffs removed, can't use skills, and doesn't gain mana.\n\n* Level Up - the number of targets increased by 1",
        "skillRoad": "Harley giantizes a nearby ally for 3s, increasing that ally's max health by  30%\n\n* Level 3 | 5: Target count increased by 1",
        "__v": 0
    },
    {
        "recommendItems": [
            31,
            40,
            30,
            42,
            35
        ],
        "recommendRunes": [
            3,
            16,
            19,
            20,
            4
        ],
        "recommendAlly": [
            15,
            8,
            5,
            10,
            7,
            18,
            3
        ],
        "recommendClass": [
            2,
            5
        ],
        "recommendOrigin": [
            1,
            4
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65f3",
        "heroID": 17,
        "attackDamage": 10,
        "attackSpeed": 0.7,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 120,
        "manaGain": 10,
        "heath": 130,
        "armor": 30,
        "magicResist": 40,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.52,
        "skillPlatform": "Ragnar restores 100 health to 2 attacking allies.\nCoop-Mode: Ragnar deals 200 damage to 2 targets.\n\n* Level 3 | 5: Target count increased by 1",
        "skillRoad": "Ragnar taunts all enemies causing them to focus on Ragnar only, and gain 30(40/50/60/70)% damage reduction from all sources for 2s.",
        "__v": 0
    },
    {
        "recommendItems": [
            29,
            24,
            18,
            33,
            25
        ],
        "recommendRunes": [
            18,
            6,
            15,
            5,
            17
        ],
        "recommendAlly": [
            3,
            6,
            13,
            7,
            12,
            20,
            9
        ],
        "recommendClass": [
            8,
            8
        ],
        "recommendOrigin": [
            10,
            6
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65f4",
        "heroID": 18,
        "attackDamage": 8,
        "attackSpeed": 0.75,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 40,
        "manaGain": 10,
        "heath": 110,
        "armor": 15,
        "magicResist": 5,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Thanatos finds a target that can be destroyed. If there is no killable target, the weakest health target will be selected.\nHe casts an ability on a selected target dealing 100 magic damage.\nIf the target is destroyed, Thatanos gains +5 AP.",
        "skillRoad": "Thatanos enchants anyone who damages him, banning attacks for 2s and forcing them to choose another target.\n(Hero whose Target Type is Front that deals damage to the leading Thatanos will not be forced to choose another target)",
        "__v": 0
    },
    {
        "recommendItems": [
            10,
            9,
            17,
            20,
            21
        ],
        "recommendRunes": [
            13,
            9,
            1,
            10,
            1
        ],
        "recommendAlly": [
            4,
            10,
            7,
            13,
            8,
            11,
            3
        ],
        "recommendClass": [
            7,
            3
        ],
        "recommendOrigin": [
            9,
            1
        ],
        "recommendTargetType": [
            4
        ],
        "_id": "61dcf70b73e1a927480c65f5",
        "heroID": 19,
        "attackDamage": 23,
        "attackSpeed": 0.7,
        "abilityPower": 0,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 0,
        "manaGain": 0,
        "heath": 130,
        "armor": 40,
        "magicResist": 40,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.5,
        "skillPlatform": "Clemens' basic attacks apply an ice effect that reduces the target's movement speed by 30(40/50/60/70)% for 2s.",
        "skillRoad": "Empty",
        "__v": 0
    },
    {
        "recommendItems": [
            17,
            18,
            24,
            10,
            9
        ],
        "recommendRunes": [
            13,
            1,
            18,
            20,
            2
        ],
        "recommendAlly": [
            15,
            7,
            13,
            3,
            14,
            2,
            6
        ],
        "recommendClass": [
            7,
            2
        ],
        "recommendOrigin": [
            5,
            3
        ],
        "recommendTargetType": [
            1
        ],
        "_id": "61dcf70b73e1a927480c65f6",
        "heroID": 20,
        "attackDamage": 6,
        "attackSpeed": 1,
        "abilityPower": 100,
        "critChance": 0.1,
        "critDamage": 1,
        "mana": 100,
        "manaGain": 10,
        "heath": 100,
        "armor": 5,
        "magicResist": 5,
        "dodgeChance": 0,
        "critBlock": 0,
        "critDefense": 0,
        "moveSpeed": 0.6,
        "skillPlatform": "Leah's basic attacks leave a feather in the path right behind her target, lasting for 10s.\nAfter having enough mana, Leah pulls all of her feathers back to her, dealing 20 damage to each enemy the feather passes through.",
        "skillRoad": "Leah levitates and becomes untargetable for 2s.",
        "__v": 0
    }
]
export default {
    HEROS,
    ITEMS,
    RUNES,
    CLASSES,
    ORIGINS,
    HERO_STATS
}