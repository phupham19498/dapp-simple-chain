import React, { useState, useCallback, useEffect } from "react"
import useToast from "hooks/useToast"
import { useTranslation } from 'contexts/Localization'
import { ToastDescriptionWithTx } from "components/Toast"
import mintNftAbi from "config/abi/mintNFT.json"
import { getAddress } from "utils/addressHelpers"
import contracts from "config/constants/contracts"
import { ethers } from "ethers"

export const useMintNFT = (account, id, amount) => {
    const { toastSuccess, toastError } = useToast()
    const { t } = useTranslation() 
    const [requestedMintNFT, setrequestedMintNFT] = useState(false)
    const [ pendingMintNft, setPendingMintNFT ] = useState(false)
    const handleMintNFT = useCallback(async () => {
      setPendingMintNFT(true)
        const { ethereum } = window
        try {
			const provider = new ethers.providers.Web3Provider(ethereum)
            const signer = provider.getSigner()
            const mintNftAddress = getAddress(contracts.mintNft)
            const contractAddress = new ethers.Contract(mintNftAddress,mintNftAbi, signer)
            const result = await contractAddress.mint(account, id, amount)
            // const receipt = result.wait()
            result.wait().then((value) => {
              if(value.status === 1){
                setPendingMintNFT(false)
                toastSuccess(
                  t('Successfully MintNFT'),
                  <ToastDescriptionWithTx txHash={value.transactionHash}/>
                )
              }
              else {
                toastError(t('Error'), t('Please try again. Confirm the transaction and make sure you are paying enough gas!'))
                setPendingMintNFT(false)
              }
          })
        } catch (e) {
            console.log(e)
            toastError(t('Error'), t('Please try again. Confirm the transaction and make sure you are paying enough gas!'))
        }
        }, [
            account, 
            id, 
            amount,
            toastError,
            toastSuccess,
            setPendingMintNFT,
            t,
          ])
    return { handleMintNFT, requestedMintNFT, pendingMintNft }
}
  