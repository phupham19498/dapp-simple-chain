
import { useEffect, useState } from 'react'
import multicall from 'utils/multicall'
import BigNumber from 'bignumber.js'
import { getAddress } from 'utils/addressHelpers'
import contract from 'config/constants/contracts'
import mintNft from "config/abi/mintNFT.json"
import { useWeb3React } from '@web3-react/core'

export const FetchDataMintRole = () => {
    const { account } = useWeb3React()
    const [mintRole, setMintRole] = useState("")
    useEffect(() => {
      const fetchMintRole = async () => {
        try {
          const calls = [{
            address: getAddress(contract.mintNft),
            name: 'MINTER_ROLE',
            params: []
          }
        ]
    
          const [result] = await multicall(mintNft, calls);
          setMintRole(result)
        } catch (e) {
          console.log(e)
        }
      }
      if ( account ) {
        fetchMintRole();
      }
    }, [account])
    return {mintRole}
}
export const FetchMintRoleUser = (mintRole) => {
    const { account } = useWeb3React()
    const [ isMintRole, setUserMintRole] = useState(false)
    useEffect(() => {
      const fetchUserMintRole = async () => {
        try {
          const calls = [{
            address: getAddress(contract.mintNft),
            name: 'hasRole',
            params: [mintRole[0],account]
          }]
          const [result] = await multicall(mintNft, calls);
          setUserMintRole(result[0])
        } catch (e) {
          console.log(e)
        }
      }
      if ( account && mintRole ) {
        fetchUserMintRole();
      }
    }, [account, mintRole])
    return {isMintRole}
}
