import React from "react";
import { Text, Flex, Button, AutoRenewIcon } from "@phamphu19498/bami-uikit"
import Page from "components/Layout/Page";
import { useTranslation } from "contexts/Localization";
import { useWeb3React } from "@web3-react/core";
import useToast from "hooks/useToast"
import { FetchDataMintRole, FetchMintRoleUser } from "./hook/fetchDataUser";
import { useMintNFT } from "./hook/useMintNft";

const Dashboard = () => {
    const { t } = useTranslation()
    const { account } = useWeb3React()
    const { mintRole } = FetchDataMintRole()
    const { isMintRole } = FetchMintRoleUser(mintRole)
    const { handleMintNFT, requestedMintNFT, pendingMintNft } = useMintNFT("0x2Dd9708bEC13d054525A9a76D325C36cE7De3895", 10000, 1)
    const { toastSuccess, toastError } = useToast()
    function alert(){
        toastSuccess(t('Error'), t('Please try again. Confirm the transaction and make sure you are paying enough gas!'))
    }
    return (
        <Page>
            <Flex flexDirection="column">
                <Text>{account}</Text>
                <Text>MINT_ROLE: {mintRole}</Text>
                {isMintRole === false ?
                    <Text>{t("You do not have permission to mint nft")}</Text>
                :
                    <Button 
                        width="300px"
                        onClick={handleMintNFT}
                        disabled={pendingMintNft}
                        endIcon={pendingMintNft ? <AutoRenewIcon spin color="textDisable" /> : null}
                    >
                        {t("Mint NFT")}
                    </Button>
                }
            </Flex>
            
        </Page>
    )
}
export default Dashboard