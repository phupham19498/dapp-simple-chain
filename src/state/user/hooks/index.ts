import { ChainId, Pair, Token } from '@pancakeswap/sdk'
import { useCallback } from 'react'
import { parseUnits } from 'ethers/lib/utils'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, AppState } from '../../index'
import { toggleTheme as toggleThemeAction} from '../actions'

export enum GAS_PRICE {
  default = '5',
  fast = '6',
  instant = '7',
  testnet = '10',
}

export const GAS_PRICE_GWEI = {
  default: parseUnits(GAS_PRICE.default, 'gwei').toString(),
  fast: parseUnits(GAS_PRICE.fast, 'gwei').toString(),
  instant: parseUnits(GAS_PRICE.instant, 'gwei').toString(),
  testnet: parseUnits(GAS_PRICE.testnet, 'gwei').toString(),
}

export function useThemeManager(): [boolean, () => void] {
    const dispatch = useDispatch<AppDispatch>()
    const isDark = useSelector<AppState, AppState['user']['isDark']>((state) => state.user.isDark)
  
    const toggleTheme = useCallback(() => {
      dispatch(toggleThemeAction())
    }, [dispatch])
  
    return [isDark, toggleTheme]
}
export function useGasPrice(): string {
  const chainId = process.env.REACT_APP_CHAIN_ID
  const userGas = useSelector<AppState, AppState['user']['gasPrice']>((state) => state.user.gasPrice)
  return chainId === ChainId.MAINNET.toString() ? userGas : GAS_PRICE_GWEI.testnet
}
