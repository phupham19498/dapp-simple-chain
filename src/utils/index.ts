import { BASE_POLYGON_SCAN_URLS } from "config/index"
import { JsonRpcSigner, Web3Provider } from '@ethersproject/providers'
import { Contract } from '@ethersproject/contracts'
import { AddressZero } from '@ethersproject/constants'
import { getAddress } from "./addressHelpers"

export function getBscScanLink(
    data: string | number,
    type: 'transaction' | 'token' | 'address' | 'block' | 'countdown',
  ): string {
    switch (type) {
      case 'transaction': {
        return `${BASE_POLYGON_SCAN_URLS}/tx/${data}`
      }
      case 'token': {
        return `${BASE_POLYGON_SCAN_URLS}/token/${data}`
      }
      case 'block': {
        return `${BASE_POLYGON_SCAN_URLS}/block/${data}`
      }
      case 'countdown': {
        return `${BASE_POLYGON_SCAN_URLS}/block/countdown/${data}`
      }
      default: {
        return `${BASE_POLYGON_SCAN_URLS}/address/${data}`
      }
    }
}
export function isAddress(value: any): string | false {
    try {
      return getAddress(value)
    } catch {
      return false
    }
}
// account is not optional
export function getSigner(library: Web3Provider, account: string): JsonRpcSigner {
    return library.getSigner(account).connectUnchecked()
}
  
export function getProviderOrSigner(library: Web3Provider, account?: string): Web3Provider | JsonRpcSigner {
    return account ? getSigner(library, account) : library
}
export function getContract(address: string, ABI: any, library: Web3Provider, account?: string): Contract {
    if (!isAddress(address) || address === AddressZero) {
      throw Error(`Invalid 'address' parameter '${address}'.`)
    }
  
    return new Contract(address, ABI, getProviderOrSigner(library, account) as any)
}