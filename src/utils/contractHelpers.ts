import { ethers } from 'ethers'
import MultiCallAbi from 'config/abi/Multicall.json'
import { simpleRpcProvider } from './providers'
import { getMulticallAddress } from './addressHelpers'

const getContract = (abi: any, address: string, signer?: ethers.Signer | ethers.providers.Provider) => {
    const signerOrProvider = signer ?? simpleRpcProvider
    return new ethers.Contract(address, abi, signerOrProvider)
}
export const getMulticallContract = (signer?: ethers.Signer | ethers.providers.Provider) => {
    return getContract(MultiCallAbi, getMulticallAddress(), signer)
}