import contracts from 'config/constants/contracts'

export const getAddress = (address) => {
  const chainId = process.env.REACT_APP_CHAIN_ID
  return address[chainId] ? address[chainId] : address[137]
}
export const getMulticallAddress = () => {
    return getAddress(contracts.multiCall)
}